%% dgg100.cls: LaTeX class for DGG100 "Geophysik im Wandel" articles
%% =================================================================================
%% Original author: Dr. Thomas Michael Hertweck, Thomas.Hertweck@kit.edu
%% Last updates:
%% v1.0, 20/08/21, TMH: Initial release
%% v1.1, 24/08/21, TMH: Updated package options to be more backward compatible.
%% v1.2, 31/08/21, TMH: Introduced doi and download macros. 
%% v1.3, 17/09/21, TMH: Incorporated feedback from RB editorial team.
%% v1.4, 23/12/21, TMH: Minor changes in "Impressum"; updated bibliography settings.
%% v1.5, 13/01/22, TMH: Updated "Impressum" with permalink for downloads; disabled
%%                      use of doi and download macros.
%% v1.6, 11/02/22, TMH: Updated bibliography settings to remove "In:" for entries of
%%                      type "article".
%% v1.7, 15/02/22, TMH: Use package "xurl" with option "hyphens" (after biblatex).
%%                      Use normal text font for URLs.
%% v1.8, 22/03/22, TMH: Handle language selection in a different way to allow mixed
%%                      English/German documents; if user specifies English, still
%%                      need German settings (cover), so make sure to load both.
%% v1.9, 29/03/22, TMH: make loading of xurl an option. Sort multi-citations in text
%%                      by year (or appearance) rather than name.
%% v2.0, 09/10/22, TMH: make space before "&" in references nonbreakable.
%% v2.1, 16/12/22, TMH: reformat biblatex German date to avoid \thinspace after
%%                      separating . in date format (suggested by Duden 1996).
%% =================================================================================

\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{dgg100}[2022/03/29 v1.9 DGG100 article]

\RequirePackage{kvoptions}
\RequirePackage[l2tabu,orthodox]{nag}
\RequirePackage{xifthen}

\RequirePackage{iftex}

% class options setup
\SetupKeyvalOptions{
  family = DGG,
  prefix = dgg@
}

% distance between columns in "twocolumn" mode; default 7mm
\DeclareStringOption[7mm]{colsep}
% width of vertical rule between columns in "twocolumn" mode; default 0pt
\DeclareStringOption[0pt]{colseprule}
% language selection; default german
\DeclareBoolOption[true]{german}
\DeclareComplementaryOption{english}{german}
% allow breaking URLs at any character; default: true
\DeclareBoolOption[true]{xurl}
% pass unknown options to underlying class
\DeclareDefaultOption{\PassOptionsToClass{\CurrentOption}{article}}
\ProcessKeyvalOptions{DGG}

% load base class
\LoadClass[a4paper,twoside]{article}

% load various macro packages
\RequirePackage[T1]{fontenc}
\ifluatex
\else
\RequirePackage[utf8]{inputenc}
\fi
\RequirePackage{microtype}
\RequirePackage{etoolbox}
\RequirePackage{xparse}
\RequirePackage{xstring}
\RequirePackage{calc}
\RequirePackage[top=1cm,
                bottom=1.3cm,
                left=2cm,
                right=2cm,
                includehead,
                includefoot]{geometry}
\ifdgg@german%
  \RequirePackage[english,main=german]{babel}
\else%
  \RequirePackage[german,main=english]{babel}
\fi
\RequirePackage{XCharter}
%\RequirePackage[uprightscript,charter,vvarbb,scaled=1.05]{newtxmath} % requires fairly new "nextxmath" package
\RequirePackage[charter,vvarbb,scaled=1.05]{newtxmath}
\RequirePackage[autostyle=true]{csquotes}
\RequirePackage{parskip}
\RequirePackage[svgnames]{xcolor}
\RequirePackage{graphicx}
\RequirePackage{wrapfig}
\RequirePackage{booktabs,multirow}
\RequirePackage{listings}
\RequirePackage{fancyhdr} 
\RequirePackage[backend=bibtex, % should use biber (UTF-8 compliant), but bibtex more backward compatible
                style=authoryear-comp,
                sorting=nyt,
                sortcites=false,
                maxnames=2,
                minnames=1,
                maxbibnames=10,
                minbibnames=3,
                abbreviate=true,
                doi=true,
                useprefix=true,
                giveninits=true, 
                uniquename=init,
                natbib=true,
                dashed=false]{biblatex}
\ifdgg@xurl%
\RequirePackage[hyphens]{xurl}
\fi
\RequirePackage{enumitem}
\RequirePackage{titling}
\RequirePackage{titlesec}
\RequirePackage[tableposition=top,
                figureposition=below]{caption}
\RequirePackage[hidelinks]{hyperref}
\urlstyle{same}

% biblatex settings
\DefineBibliographyStrings{german}{ 
   andothers = {et\addabbrvspace al\adddot},
   andmore   = {et\addabbrvspace al\adddot},
}
\renewcommand{\finalnamedelim}{\addnbspace\&\space}
\DeclareNameAlias{sortname}{family-given}
\renewbibmacro{in:}{%
  \ifentrytype{article}{}{\printtext{\bibstring{in}\intitlepunct}}%
}

\DefineBibliographyExtras{german}{% from german.lbx
  \protected\def\mkbibdateshort#1#2#3{%
  \iffieldundef{#3}
  {}
  {\mkdayzeros{\thefield{#3}}\adddot
    \iffieldundef{#2}{}{}}%<-\thinspace removed
  \iffieldundef{#2}
  {}
  {\mkmonthzeros{\thefield{#2}}%
    \iffieldundef{#1}
    {}
    {\iffieldundef{#3}{/}{\adddot}}}%<-\thinspace removed
  \iffieldbibstring{#1}
  {\bibstring{\thefield{#1}}}
  {\dateeraprintpre{#1}\mkyearzeros{\thefield{#1}}}}%
}

% define additional macros
\def\affiliation#1{\gdef\dgg@inst{#1}}
\def\dgg@inst{\@latex@error{No \noexpand\affiliation given}\@ehc}
\def\shorttitle#1{\gdef\dgg@shorttitle{#1}}
\def\dgg@shorttitle{\thetitle}
\def\doi#1{\gdef\dgg@doi{#1}}
\def\dgg@doi{}
\def\download#1{\gdef\dgg@download{#1}}
\def\dgg@download{}

% adjust twocolumn layout
\setlength{\columnsep}{\dgg@colsep} 
\setlength{\columnseprule}{\dgg@colseprule}

% define color(s)
\definecolor{DGGred}{RGB}{196,16,18}
\definecolor{DGGcover}{RGB}{230,230,230}

% fancyhdr settings
\setlength{\headheight}{20pt}
\pagestyle{fancy}
\renewcommand{\headrulewidth}{0.0pt}
\renewcommand{\footrulewidth}{0.4pt}
\fancyhf{}
\fancyhead[L]{\textit{Deutsche Geophysikalische Gesellschaft e.V.}}
\fancyhead[R]{\textit{Schriftenreihe \enquote{Geophysik im Wandel}}}
\fancyhead[C]{\hspace*{8mm}\raisebox{-1ex}{\href{https://dgg-online.de/}{\includegraphics[height=7mm]{Fig/DGG100grau}}}}
\fancyfoot[LO,RE]{\textit{\thepage}}
\fancyfoot[LE,RO]{\textit{\thedate}}
\fancyfoot[C]{\textit{\dgg@shorttitle}}
\fancypagestyle{plain}{ 
  \fancyhf{}
  \fancyhead[L]{\textit{Deutsche Geophysikalische Gesellschaft e.V.}}
  \fancyhead[R]{\textit{Schriftenreihe \enquote{Geophysik im Wandel}}}
  \fancyhead[C]{\hspace*{8mm}\raisebox{-1ex}{\href{https://dgg-online.de/}{\includegraphics[height=7mm]{Fig/DGG100grau}}}}
  \fancyfoot[LO,RE]{\textit{\thepage}}
  \fancyfoot[LE,RO]{\textit{\thedate}}
  \fancyfoot[C]{}
}

% titling settings
\renewcommand{\droptitle}{-9mm}
\renewcommand{\maketitlehooka}{\textcolor{DGGred}{\rule{\linewidth}{3pt}}\par}
\pretitle{\begin{flushleft}\Huge\color{DGGred}}
\posttitle{\par\end{flushleft}\vskip 1.5em}
\preauthor{\begin{flushleft}\large\textbf}
\postauthor{\end{flushleft}\par\vskip 0.3em}
\renewcommand{\maketitlehookc}{\dgg@inst}
\predate{\let\@date\@empty}
\postdate{\par}
\renewcommand{\maketitlehookd}{\textcolor{DGGred}{\rule{\linewidth}{3pt}}}

% enumitem settings
\setlist[itemize]{noitemsep}

% titlesec settings
\titleformat{\section}[hang]{\bfseries}{\thesection.}{0.5em}{} 
\titleformat{\subsection}[runin]{\bfseries}{\thesubsection.}{0.3em}{} 
\titleformat{\subsubsection}[runin]{\bfseries}{\thesubsubsection.}{0.3em}{} 
\titlespacing{\section}{0pt}{*1}{*0.5}
\titlespacing{\subsection}{0pt}{*0.5}{*1}
\titlespacing{\subsubsection}{0pt}{*0.5}{*1}

% caption settings
\captionsetup{format=plain,
              labelformat=default, % should change to "original" but requires fairly new "caption" package
              labelfont={bf},
              font={it,normalsize},
              labelsep=colon,
              justification=justified,
              singlelinecheck=true,
              margin=0pt}
\captionsetup[table]{skip=3pt}
\captionsetup[figure]{skip=8pt}

% cover page for impress
\NewDocumentCommand{\makecover} {} {%
\clearpage\onecolumn
\begin{otherlanguage}{german}
\thispagestyle{empty}
\pagecolor{DGGcover} 
\vspace*{1cm}
\textbf{\large Impressum}\\[5mm]
\begin{minipage}{0.2\linewidth}
\centering
\href{https://dgg-online.de/}{\includegraphics[height=2cm]{Fig/DGGLogo}}
\end{minipage}
\hfill
\begin{minipage}{0.75\linewidth}
\begin{tabular}{ll}
Herausgeber: & Deutsche Geophysikalische Gesellschaft e.V. (DGG)\\
Gesch\"aftsstelle: & Bundesanstalt f\"ur Geowissenschaften und Rohstoffe\\
                   & Stilleweg 2, 30655 Hannover\\
Redaktion: & Komitee DGG100\\
           & E-Mail \href{mailto:dgg100@dgg-online.de}{dgg100@dgg-online.de}\\
           & Internet \href{https://dgg-online.de/}{https://dgg-online.de/}
\end{tabular}
\end{minipage}
\vspace*{5mm}

Beitr\"age f\"ur die DGG-Schriftenreihe \enquote{Geophysik im Wandel} sind aus allen Bereichen der Geophysik und der angrenzenden Fachgebiete erw\"unscht. F\"ur den Inhalt der Beitr\"age sind die Autorinnen und Autoren verantwortlich. Bitte beachten Sie, dass die namentlich gekennzeichneten Beitr\"age pers\"onliche Meinungen bzw.\ Ansichten enthalten k\"onnen, die nicht mit der Meinung oder Ansicht des Herausgebers, des Vorstands der Deutschen Geophysikalischen Gesellschaft e.V.\ oder der Redaktion \"ubereinstimmen m\"ussen. Die Autorinnen und Autoren erkl\"aren gegen\"uber der Redaktion, dass sie \"uber die Vervielf\"altigungsrechte aller Fotos und Abbildungen innerhalb ihrer Beitr\"age verf\"ugen.\\

\vfill
%\ifdefempty{\dgg@download}{}{%
%\begin{tabular}{ll}
%Download-URL dieses Artikels: & \href{\dgg@download}{\dgg@download}\\
%\end{tabular}}
%\ifdefempty{\dgg@doi}{}{%
%\begin{tabular}{ll}
%Digital Object Identifier (DOI): & \href{\dgg@doi}{\dgg@doi}\\
%\end{tabular}}
Hinweis: Diese PDF-Datei sowie weitere Artikel der DGG-Schriftenreihe \enquote{Geophysik im Wandel} stehen unter \href{https://dgg-online.de/dgg100-geophysik-im-wandel/}{https://dgg-online.de/dgg100-geophysik-im-wandel/} zum Download zur Verf\"ugung.
\end{otherlanguage}
}

% inline impress
\NewDocumentCommand {\impress} {s O{1.5cm}} {%
\begin{otherlanguage}{german}
\section*{Impressum}
Herausgeber: Deutsche Geophysikalische Gesellschaft e.V. (DGG). Gesch\"aftsstelle: Bundesanstalt f\"ur Geowissenschaften und Rohstoffe, Stilleweg 2, 30655 Hannover.
Redaktion: Komitee DGG100, E-Mail \href{mailto:dgg100@dgg-online.de}{dgg100@dgg-online.de}, Internet \href{https://dgg-online.de/}{https://dgg-online.de/}.
\IfBooleanTF{#1}{\par}{\begin{center}\href{https://dgg-online.de/}{\includegraphics[height=#2]{Fig/DGGLogo}}\end{center}}
Beitr\"age f\"ur die DGG-Schriftenreihe \enquote{Geophysik im Wandel} sind aus allen Bereichen der Geophysik und der angrenzenden Fachgebiete erw\"unscht. F\"ur den Inhalt der Beitr\"age sind die Autorinnen und Autoren verantwortlich. Bitte beachten Sie, dass die namentlich gekennzeichneten Beitr\"age pers\"onliche Meinungen bzw.\ Ansichten enthalten k\"onnen, die nicht mit der Meinung oder Ansicht des Herausgebers, des Vorstands der Deutschen Geophysikalischen Gesellschaft e.V.\ oder der Redaktion \"ubereinstimmen m\"ussen. Die Autorinnen und Autoren erkl\"aren gegen\"uber der Redaktion, dass sie \"uber die Vervielf\"altigungsrechte aller Fotos und Abbildungen innerhalb ihrer Beitr\"age verf\"ugen. Hinweis: Diese PDF-Datei sowie weitere Artikel der DGG-Schriftenreihe \enquote{Geophysik im Wandel} stehen unter \href{https://dgg-online.de/dgg100-geophysik-im-wandel/}{https://dgg-online.de/dgg100-geophysik-im-wandel/} zum Download zur Verf\"ugung.%
\end{otherlanguage}
}
