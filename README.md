## Name
DGG100 Template

## Description
A LaTeX template for articles of the Deutsche Geophysikalische Gesellschaft (DGG) series "Geophysik im Wandel" as part of the celebrations on the occasion of DGG's 100th birthday.

## Installation
Simply download or clone this repository and start using the template. A LIESMICH.txt file with instructions (in German) is included. An example (beispiel_artikel.tex) is also available.

## Support
If technical issues are encountered when using this template, or if there are suggestions or ideas for improvement, you can get in touch with the author at Thomas.Hertweck@kit.edu.

## License
(c)2021-2023.
